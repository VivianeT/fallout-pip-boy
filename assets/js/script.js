/********************************
*                               *
*-----------NAVIGATION----------*
*                               *
*********************************/

//Target element of DOM

//For the nav 
let navStat = document.querySelector('#navStat');
let navInv = document.querySelector('#navInv');
let navData = document.querySelector('#navData');
let navMap = document.querySelector('#navMap');
let navRadio = document.querySelector('#navRadio');

//For the sections
let sectionStat = document.querySelector('#stat');
let sectionInv = document.querySelector('#inv');
let sectionData = document.querySelector('#data');
let sectionMap = document.querySelector('#map');
let sectionRadio = document.querySelector('#radio');

//For greenLine
let greenNavStat = document.querySelector('.navStat');
let greenNavInv = document.querySelector('.navInv');
let greenNavData = document.querySelector('.navData');
let greenNavMap = document.querySelector('.navMap');
let greenNavRadio = document.querySelector('.navRadio');

//Give a css parameter for the green line for each li

function greenLine(greenLineDiv, navLi) {
    greenLineDiv.style.width = navLi.offsetWidth+1 + "px";
}
greenLine(greenNavStat, navStat);
greenLine(greenNavInv, navInv);
greenLine(greenNavData, navData);
greenLine(greenNavMap, navMap);
greenLine(greenNavRadio, navRadio);

//Create a function for change contain

function changeContainNav(newContain, oldContain1, oldContain2, oldContain3, oldContain4, currentNav, oldNav1, oldNav2, oldNav3, oldNav4) {
    //Remove the d-none class for the new contain
    newContain.classList.remove('d-none');

    //Add d-none for oldContain1
    oldContain1.classList.add("d-none");
    oldContain2.classList.add("d-none");
    oldContain3.classList.add("d-none");
    oldContain4.classList.add("d-none");

    //Add active class for the current contain
    currentNav.classList.add("active");

    //Remove active class for the old contain 
    oldNav1.classList.remove("active");
    oldNav2.classList.remove("active");
    oldNav3.classList.remove("active");
    oldNav4.classList.remove("active");

    //Refresh the value for greenLine
    greenLine(greenNavStat, navStat);
    greenLine(greenNavInv, navInv);
    greenLine(greenNavData, navData);
    greenLine(greenNavMap, navMap);
    greenLine(greenNavRadio, navRadio);
}

//Use the fonction for each click on the navbar
navStat.addEventListener('click', function () {
    changeContainNav(sectionStat, sectionInv, sectionData, sectionMap, sectionRadio, navStat, navInv, navData, navMap, navRadio);
})
navInv.addEventListener('click', function () {
    changeContainNav(sectionInv, sectionStat, sectionData, sectionMap, sectionRadio, navInv, navStat, navData, navMap, navRadio);
})
navData.addEventListener('click', function () {
    changeContainNav(sectionData, sectionStat, sectionInv, sectionMap, sectionRadio, navData, navStat, navInv, navMap, navRadio);
})
navMap.addEventListener('click', function () {
    changeContainNav(sectionMap, sectionStat, sectionInv, sectionData, sectionRadio, navMap, navStat, navInv, navData, navRadio);
})
navRadio.addEventListener('click', function () {
    changeContainNav(sectionRadio, sectionStat, sectionInv, sectionData, sectionMap, navRadio, navStat, navInv, navData, navMap);
})

/********************************
*                               *
*------SECONDE--NAVIGATION------*
*                               *
*********************************/

//STAT SECTION

//Target button and div
let navStatus = document.querySelector('#navStatus');
let navSpecial = document.querySelector('#navSpecial');
let navPerks = document.querySelector('#navPerks');

let statusDiv = document.querySelector('#statusDiv');
let speciaDiv = document.querySelector('#specialDiv');
let perksDiv = document.querySelector('#perksDiv');

//Add an eventListener for each click on the navbar
navStatus.addEventListener('click', function(){
    //Add d-none class for other class and remove this class for this div
    statusDiv.classList.remove('d-none');
    speciaDiv.classList.add('d-none');
    perksDiv.classList.add('d-none');

    //Add activeNav class for the current button et remove this class for other
    navStatus.classList.add('activeNav');
    navSpecial.classList.remove('activeNav');
    navPerks.classList.remove('activeNav');

});

navSpecial.addEventListener('click', function(){
    //Add d-none class for other class and remove this class for this div
    statusDiv.classList.add('d-none');
    speciaDiv.classList.remove('d-none');
    perksDiv.classList.add('d-none');

    //Add activeNav class for the current button et remove this class for other
    navStatus.classList.remove('activeNav');
    navSpecial.classList.add('activeNav');
    navPerks.classList.remove('activeNav');

});

navPerks.addEventListener('click', function(){
    //Add d-none class for other class and remove this class for this div
    statusDiv.classList.add('d-none');
    speciaDiv.classList.add('d-none');
    perksDiv.classList.remove('d-none');

    //Add activeNav class for the current button et remove this class for other
    navStatus.classList.remove('activeNav');
    navSpecial.classList.remove('activeNav');
    navPerks.classList.add('activeNav');

});

// INV SECTION


//DATA SECTION


//MAP SECTION


//RADIO SECTION